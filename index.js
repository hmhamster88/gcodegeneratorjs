const express = require('express');

const args = process.argv;
let port = 8082;
if (args.length > 2) {
  port = args[2];
}
const app = express();
app.use('/', express.static('./dist'));
app.listen(port, function () {
  console.log(`Example app listening on port ${port}!`);
});
