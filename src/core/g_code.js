/**
 * Created by stanislavbatylin on 03/07/17.
 */

import RotateDirection from './geometry/rotate_direction';
import Angle from './geometry/angle';
import {Vector3, Vector2} from 'three';

export default class GCode {
  static rapidMove (x, y, z) {
    const code = new GCode();
    code.g = 0;
    code.x = x;
    code.y = y;
    code.z = z;
    return code;
  }

  static move (x, y, z) {
    const code = new GCode();
    code.g = 1;
    code.x = x;
    code.y = y;
    code.z = z;
    return code;
  }

  static arc (x, y, z, i, j, direction) {
    const code = new GCode();
    code.g = direction === RotateDirection.CW ? 2 : 3;
    code.x = x;
    code.y = y;
    code.z = z;
    code.i = i;
    code.j = j;
    return code;
  }

  static absoluteCoords () {
    const code = new GCode();
    code.g = 98;
    return code;
  }

  toString () {
    let string = '';
    let comment = '';
    for (const prop in this) {
      if (prop === comment) {
        comment = this[prop];
      }
      if (!isNaN(this[prop])) {
        const value = (prop === 'g' || prop === 'm') ? ('0' + this[prop]).slice(-2) : this[prop].toFixed(4);
        string += prop.toUpperCase() + value + ' ';
      }
    }
    return string + comment;
  }

  getPointsForArc (initPos) {
    const rotateDirection = this.g === 2 ? RotateDirection.CW : RotateDirection.CCW;
    const start = new Vector2(initPos.x, initPos.y);
    const finish = new Vector2(this.x, this.y);
    const ij = new Vector2(this.i, this.j);
    const center = start.clone().add(ij);
    const radius = ij.length();
    const startAngle = start.clone().sub(center).angle();
    const stopAngle = finish.clone().sub(center).angle();
    let distance = rotateDirection * (stopAngle - startAngle);
    if (distance < 0) {
      distance += Math.PI * 2;
    }
    if (distance === 0) {
      distance = Math.PI * 2;
    }
    const steps = 40;
    const step = distance / steps;
    const points = [];
    for (let i = 0; i < steps; i++) {
      const angle = new Angle(startAngle + step * i * rotateDirection);
      const td = center.clone().add(angle.vector.multiplyScalar(radius));
      points.push(new Vector3(td.x, td.y, initPos.z));
    }
    points.unshift(initPos.clone());
    points.push(new Vector3(finish.x, finish.y, initPos.z));
    return points;
  }

  getPoints (initPos) {
    switch (this.g) {
      case 0:
      case 1:
        return [initPos.clone(), new Vector3(this.x, this.y, this.z)];
      case 2:
      case 3:
        return this.getPointsForArc(initPos);
    }
  }
}
