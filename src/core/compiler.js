/**
 * Created by stanislavbatylin on 21/06/17.
 */

/* eslint-disable no-unused-vars */
/* eslint-disable prefer-const */

import Contour from './geometry/contour';
import Part from './geometry/part';
import * as THREE from 'three';
import PartBuilder from './geometry/part_builder';
import GCode from './g_code';
import RotateDirection from './geometry/rotate_direction';

function defaultGenerator () {
  return {parts: [], codes: []};
}

function scope (source, partBuilder, three, contour, part, gcode, rotateDirection) {
  'use strict';
  const Contour = contour;
  const Part = part;
  const THREE = three;
  const PartBuilder = partBuilder;
  const pb = new PartBuilder();
  const GCode = gcode;
  const RD = rotateDirection;
  const RotateDirection = rotateDirection;
  let defaultParams = {};
  let generator = defaultGenerator; // eslint-disable-line no-unused-vars
  eval(source);// eslint-disable-line no-eval
  return {defaultParams: defaultParams, generator: generator};
};

export default class Compiler {
  constructor () {
    this.error = null;
  }
  compile (source) {
    try {
      const result = scope(source, PartBuilder, THREE, Contour, Part, GCode, RotateDirection);
      return result;
    } catch (error) {
      this.error = error;
    }
    return {defaultParams: {}, generator: defaultGenerator};
  }
}
