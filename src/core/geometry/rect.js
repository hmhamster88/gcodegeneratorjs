/**
 * Created by hmham on 5/16/2017.
 */
import { Vector2 } from 'three';

export default class Rect {
  static boundingBox (points) {
    if (!points.length) {
      return null;
    }
    const min = points[0].clone();
    const max = points[0].clone();
    for (const point of points) {
      if (point.x < min.x) {
        min.x = point.x;
      } else if (point.x > max.x) {
        max.x = point.x;
      }
      if (point.y < min.y) {
        min.y = point.y;
      } else if (point.y > max.y) {
        max.y = point.y;
      }
    }
    return new Rect(min, max.clone().sub(min));
  }

  constructor (position, size) {
    this.position = position || new Vector2();
    this.size = size || new Vector2();
  }
}