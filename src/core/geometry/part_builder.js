/**
 * Created by stanislavbatylin on 31/05/17.
 */
import Contour from './contour';
import Part from './part';
import Pocket from './pocket';
import Point from './point';
import Arc from './arc';
import { Vector2 } from 'three';

export default class PartBuilder {
  constructor () {
    this.currentContour = new Contour();
    this.partContour = new Contour();
    this.pockets = [];
  }

  contour () {
    this.partContour = this.currentContour;
    this.currentContour = new Contour();
    return this;
  }

  pocket (depth) {
    this.pockets.push(new Pocket(this.currentContour, depth));
    this.currentContour = new Contour();
    return this;
  }

  addContourPart (part) {
    this.currentContour.parts.push(part);
    return this;
  }

  point (x, y) {
    this.pointV(new Vector2(x, y));
    return this;
  }

  pointV (v) {
    this.addContourPart(new Point(v));
    return this;
  }

  arcV (center, radius, start, stop, direction) {
    this.addContourPart(new Arc(center, radius, start, stop, direction));
    return this;
  }

  arc (x, y, radius, start, stop, direction) {
    this.arc(new Vector2(x, y), radius, start, stop, direction);
    return this;
  }

  addContourParts (contour) {
    for (const part of contour.parts) {
      this.addContourPart(part);
    }
    return this;
  }

  rect (width, height, radius) {
    this.addContourParts(Contour.rect(width, height, radius));
    return this;
  }

  circle (radius) {
    this.addContourParts(Contour.circle(radius));
    return this;
  }

  moveV (vector) {
    this.currentContour.move(vector);
    return this;
  }

  move (x, y) {
    this.moveV(new Vector2(x, y));
    return this;
  }

  moveToCenter () {
    this.currentContour.moveToCenter();
    return this;
  }

  moveToPositive () {
    this.currentContour.moveToPositive();
    return this;
  }

  build (thickness) {
    const part = new Part(this.partContour, this.pockets, thickness);
    this.partContour = new Contour();
    this.pockets = [];
    this.currentContour = new Contour();
    return part;
  }
}