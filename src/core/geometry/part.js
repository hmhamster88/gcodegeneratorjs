/**
 * Created by hmham on 5/17/2017.
 */
import Contour from './contour';

import { Geometry, Face3, Vector3 } from 'three';
import earcut from 'earcut';

export default class Part {
  constructor (contour, pockets, thickness) {
    this.thickness = thickness || 0;
    this.contour = contour || new Contour();
    this.pockets = pockets || [];
  }

  get drawContours () {
    const contours = [];
    contours.push(this.contour.drawPoints3D(0));
    contours.push(this.contour.drawPoints3D(this.thickness));
    for (const pocket of this.pockets) {
      contours.push(pocket.contour.drawPoints3D(this.thickness));
      if (pocket.depth) {
        contours.push(pocket.contour.drawPoints3D(this.thickness - pocket.depth));
      } else {
        contours.push(pocket.contour.drawPoints3D(0));
      }
    }
    return contours;
  }

  addPlane (geometry, contour, holes, z, reverse) {
    const flatPoints = [];
    const pointIndex = geometry.vertices.length;
    for (const point of contour) {
      geometry.vertices.push(new Vector3(point.x, point.y, z));
      flatPoints.push(point.x, point.y);
    }

    const holesIndices = [];
    for (const hole of holes) {
      holesIndices.push(flatPoints.length / 2);
      for (const point of hole) {
        geometry.vertices.push(new Vector3(point.x, point.y, z));
        flatPoints.push(point.x, point.y);
      }
    }
    const res = earcut(flatPoints, holesIndices);
    for (let i = 0; i < res.length; i += 3) {
      if (reverse) {
        geometry.faces.push(new Face3(pointIndex + res[i], pointIndex + res[i + 2], pointIndex + res[i + 1]));
      } else {
        geometry.faces.push(new Face3(pointIndex + res[i], pointIndex + res[i + 1], pointIndex + res[i + 2]));
      }
    }
  }

  toGeometry () {
    const geometry = new Geometry();
    this.contour.addVerticalGeometry(geometry, 0, this.thickness);
    const upHoles = [];
    const downHoles = [];
    for (const pocket of this.pockets) {
      const throughHole = !pocket.depth || pocket.depth > this.thickness;
      const z0 = throughHole ? 0 : this.thickness - pocket.depth;
      pocket.contour.addVerticalGeometry(geometry, z0, this.thickness, true);
      const points = pocket.contour.drawPoints;
      upHoles.push(points);
      if (throughHole) {
        downHoles.push(points);
      } else {
        this.addPlane(geometry, pocket.contour.drawPoints, [], z0);
      }
    }

    this.addPlane(geometry, this.contour.drawPoints, downHoles, 0, true);
    this.addPlane(geometry, this.contour.drawPoints, upHoles, this.thickness);

    geometry.computeVertexNormals();
    return geometry;
  }
}
