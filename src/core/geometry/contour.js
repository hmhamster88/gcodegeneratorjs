/**
 * Created by hmham on 5/16/2017.
 */
import Arc from './arc';
import Point from './point';
import Angle from './angle';
import RotateDirection from './rotate_direction';
import Rect from './rect';
import Pocket from './pocket';
import GCode from '../g_code';

import { Vector2, Vector3, Face3 } from 'three';

export default class Contour {
  static rect (width, height, radius) {
    if (radius) {
      return new Contour([
        new Arc(new Vector2(radius, radius), radius, Angle.down, Angle.left, RotateDirection.CW),
        new Arc(new Vector2(radius, height - radius), radius, Angle.left, Angle.up, RotateDirection.CW),
        new Arc(new Vector2(width - radius, height - radius), radius, Angle.up, Angle.right, RotateDirection.CW),
        new Arc(new Vector2(width - radius, radius), radius, Angle.right, Angle.down, RotateDirection.CW)
      ]);
    }
    return new Contour([
      new Point(new Vector2(0, 0)),
      new Point(new Vector2(0, height)),
      new Point(new Vector2(width, height)),
      new Point(new Vector2(width, 0))
    ]);
  }

  static circle (radius, direction) {
    return new Contour([new Arc(new Vector2(), radius, 0, 0, direction || RotateDirection.CW)]);
  }

  constructor (parts, closed) {
    this.parts = parts || [];
    this.closed = closed === undefined ? true : closed;
  }

  get drawPoints () {
    let points = [];
    for (const part of this.parts) {
      points = points.concat(part.drawPoints);
    }
    if (this.closed && this.parts.length != null) {
      points.push(this.parts[0].firstPoint);
    }
    return points;
  }

  drawPoints3D (z) {
    return this.drawPoints.map(p => new Vector3(p.x, p.y, z));
  }

  get boundingPoints () {
    return this.parts.map(part => part.boundingPoints).reduce((a, b) => a.concat(b));
  }

  get boundingBox () {
    return Rect.boundingBox(this.boundingPoints);
  }

  move (vector) {
    this.parts.forEach(part => part.move(vector));
    return this;
  }

  moveToCenter () {
    const rect = this.boundingBox;
    this.move(rect.position.add(rect.size.divideScalar(2)).negate());
    return this;
  }

  moveToPositive () {
    const rect = this.boundingBox;
    this.move(rect.position.negate());
    return this;
  }

  toPocket (depth) {
    return new Pocket(this, depth);
  }

  /**
   * @param {Geometry} geometry
   */
  addVerticalGeometry (geometry, z0, z1, reverse) {
    const points = this.drawPoints;
    let pointIndex = geometry.vertices.length;
    for (const point of points) {
      geometry.vertices.push(
          new Vector3(point.x, point.y, z0),
          new Vector3(point.x, point.y, z1)
      );
      if (reverse) {
        geometry.faces.push(
            new Face3(pointIndex + 0, pointIndex + 3, pointIndex + 1),
            new Face3(pointIndex + 0, pointIndex + 2, pointIndex + 3),
        );
      } else {
        geometry.faces.push(
            new Face3(pointIndex + 0, pointIndex + 1, pointIndex + 3),
            new Face3(pointIndex + 0, pointIndex + 3, pointIndex + 2),
        );
      }
      pointIndex += 2;
    }
    geometry.faces.splice(geometry.faces.length - 2, 2);
  }

  toGCodes () {
    if (this.parts.length < 1) {
      return [];
    }
    const codes = this.parts.map(part => part.toGCodes()).reduce((a, b) => a.concat(b));
    if (!this.closed) {
      return codes;
    }
    const start = this.parts[0].firstPoint;
    codes.push(GCode.move(start.x, start.y));
    return codes;
  }
}
