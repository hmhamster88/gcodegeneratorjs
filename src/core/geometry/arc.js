/**
 * Created by hmham on 5/10/2017.
 */
import { Vector2 } from 'three';
import RotateDirection from './rotate_direction';
import Angle from './angle';
import GCode from '../g_code';

export default class Arc {
  constructor (center, radius, start, stop, direction) {
    this.center = center || new Vector2();
    this.radius = radius || 1;
    this.start = start || Angle.right;
    this.stop = stop || Angle.right;
    this.direction = direction || RotateDirection.CCW;
  }

  get fullCircle () {
    return this.start === this.stop;
  }

  get drawPoints () {
    let distance = this.direction === RotateDirection.CCW
      ? this.stop.value - this.start.value : this.start.value - this.stop.value;
    if (distance === 0) {
      distance = Math.PI * 2;
    }
    if (distance < 0) {
      distance += Math.PI * 2;
    }
    const steps = 40;
    const step = distance / 40;
    const points = [];
    for (let i = 0; i <= steps; i++) {
      const angle = new Angle(this.start.value + step * i * this.direction);
      points.push(this.center.clone().add(angle.vector.multiplyScalar(this.radius)));
    }
    return points;
  }

  get firstPoint () {
    return this.pointAtAngle(this.start);
  }

  get lastPoint () {
    return this.pointAtAngle(this.stop);
  }

  get boundingPoints () {
    const points = [];
    points.push(this.firstPoint);
    points.push(this.lastPoint);
    const angles = [Angle.right, Angle.up, Angle.left, Angle.down];
    for (const a of angles) {
      if ((a > this.start.value && a < this.stop.value) || this.fullCircle) {
        points.push(this.pointAtAngle(a));
      }
    }
    return points;
  }

  pointAtAngle (angle) {
    return this.center.clone().add(angle.vector.multiplyScalar(this.radius));
  }

  move (vector) {
    this.center.add(vector);
  }

  toGCodes () {
    const start = this.firstPoint;
    const startOffset = this.center.clone().sub(start);
    const end = this.lastPoint;
    return [
      GCode.move(start.x, start.y),
      GCode.arc(end.x, end.y, undefined, startOffset.x, startOffset.y, this.direction)
    ];
  }
}