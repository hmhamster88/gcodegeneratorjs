/**
 * Created by hmham on 5/16/2017.
 */
import Contour from './contour';

export default class Pocket {
  constructor (contour, depth) {
    this.contour = contour || new Contour();
    this.depth = depth || null;
  }
}

