/**
 * Created by hmham on 5/10/2017.
 */
import { Vector2 } from 'three';

export default class Angle {
  static get left () {
    return new Angle(Math.PI);
  }

  static get up () {
    return new Angle(Math.PI * 0.5);
  }

  static get right () {
    return new Angle(0);
  }

  static get down () {
    return new Angle(Math.PI * 1.5);
  }

  constructor (value) {
    this.value = value || 0;
  }

  get vector () {
    return new Vector2(Math.cos(this.value), Math.sin(this.value));
  }
}
