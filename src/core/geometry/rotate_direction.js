/**
 * Created by hmham on 5/10/2017.
 */
const RotateDirection = {
  CW: -1,
  CCW: 1
};
export default RotateDirection;
