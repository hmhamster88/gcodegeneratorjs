/**
 * Created by hmham on 5/16/2017.
 */
import GCode from '../g_code';
import { Vector2 } from 'three';

export default class Point {
  constructor (position) {
    this.position = position || new Vector2();
  }

  get drawPoints () {
    return [this.position];
  }

  get firstPoint () {
    return this.position.clone();
  }

  get lastPoint () {
    return this.position.clone();
  }

  get boundingPoints () {
    return this.position.clone();
  }

  move (vector) {
    this.position.add(vector);
  }

  toGCodes () {
    return [GCode.move(this.position.x, this.position.y)];
  }
}
