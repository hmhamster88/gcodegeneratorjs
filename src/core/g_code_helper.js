/**
 * Created by stanislavbatylin on 03/07/17.
 */

import GCodeHelperParams from './g_code_helper_params';

export default class GCodeHelper {
  constructor () {
    this.params = new GCodeHelperParams();
  }
};
