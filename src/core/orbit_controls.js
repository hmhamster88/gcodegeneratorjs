import * as THREE from 'three';

export default class OrbitControls {
  constructor (object, domElement) {
    this.object = object;
    this.domElement = domElement || document;
    this.spherical = new THREE.Spherical(80, 1, 1);
    this.mouseDown = false;
    this.lastPoint = null;
    this.domElement.addEventListener('mousedown', (event) => this.onMouseDown(event), false);
    this.domElement.addEventListener('mousemove', (event) => this.onMouseMove(event), false);
    this.domElement.addEventListener('mouseup', (event) => this.onMouseUp(event), false);
    this.domElement.addEventListener('wheel', (event) => this.onMouseWheel(event), false);
    this.update();
  }

  onMouseDown (event) {
    this.mouseDown = true;
  }

  onMouseUp (event) {
    this.mouseDown = false;
    this.lastPoint = null;
  }

  onMouseMove (event) {
    if (!this.mouseDown) {
      return;
    }
    const point = new THREE.Vector2(event.clientX, event.clientY);
    if (this.lastPoint) {
      const delta = point.clone().sub(this.lastPoint);
      delta.divideScalar(100);
      this.spherical.phi -= delta.y;
      this.spherical.theta += delta.x;
    }
    this.lastPoint = point;
    this.update();
  }

  onMouseWheel (event) {
    if (event.deltaY < 0) {
      this.spherical.radius /= 2;
    } else {
      this.spherical.radius *= 2;
    }
    this.update();
  }

  setFromSpherical (v, s) {
    const sinPhiRadius = Math.sin(s.phi) * s.radius;
    v.x = sinPhiRadius * Math.sin(s.theta);
    v.y = sinPhiRadius * Math.cos(s.theta);
    v.z = Math.cos(s.phi) * s.radius;
  }

  update () {
    this.setFromSpherical(this.object.position, this.spherical);
    this.object.lookAt(new THREE.Vector3());
  }
};