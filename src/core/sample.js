/**
 * Created by stanislavbatylin on 21/06/17.
 */

defaultParams = {
  width: 40,
  height: 30
};
generator = (params) => {
  const part = pb.rect(params.width, params.height, 5)
      .moveToCenter()
      .contour()
      .rect(10, 10, 2)
      .moveToCenter()
      .pocket(4)
      .circle(3)
      .move(15, 10)
      .pocket()
      .build(10);
  return {
    parts: [part],
    codes: []
  };
};
