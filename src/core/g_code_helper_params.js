/**
 * Created by stanislavbatylin on 03/07/17.
 */

export default class GCodeHelperParams {
  constructor () {
    this.materialHeight = 8;
    this.safetyHeight = 4;
    this.tolerance = 0;
    this.toolRadius = 1;
    this.rapidMoveRate = 1000;
    this.horizontalFeedRate = 200;
    this.verticalFeedRate = 100;
    this.verticalStep = 2;
    this.bridgeWidth = 2;
    this.bridgeHeidht = 0.5;
    this.bridgeCount = 1;
    this.maxRpm = 1000;
    this.laserMode = false;
    this.laserContourRepeatCount = 1;
    this.partContourOptimization = true;
  }
};
