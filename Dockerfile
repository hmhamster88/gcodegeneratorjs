FROM node:7.10.0-alpine

COPY . .

RUN npm install
RUN npm run build

#CMD ["node", "index.js"]
CMD ["sh", "run.sh"]
